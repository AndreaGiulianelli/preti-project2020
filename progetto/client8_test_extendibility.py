#ADDITIONAL CLIENT CONNECTED TO INTERFACE 192.168.2.1 OF ROUTER3 TO PROVE THE EXTENDIBILITY
from socket import *
from core.client import Client

ip = "192.168.2.2"
mac = "44:AA:AA:DA:11:AC"
router_ip = "192.168.2.1"
router_mac = "34:10:FA:3F:12:AA"
router_port = 8400

client = Client(ip, mac, router_ip, router_mac, router_port)
client.start()