'''
Module with general utilities for handling messages.

'''
from socket import inet_aton, inet_ntoa
from binascii import unhexlify, hexlify

#The lenght of the message lenght field in the header.
H_MSGLEN = 10
#The lenght of the type field in the header.
H_TYPELEN = 1
#MSG_TYPES
#New client
NEW_CLIENT = 0
#Client disconnected
CLIENT_DISC = 1
#New message
NEW_MSG = 2
#Error message
ERR_MSG = 3
#Client Addresses
CLIENT_A = 8
#Router Addresses
ROUTER_A = 9


def create_message(mac_src, mac_dst, ip_src, ip_dst, msg_type, msg):
    '''
    All the parameters are strings, so I need to conver the IP and
    the MAC adress in byte form. 
    Also IP and MAC are represented in regular form: 
    - 4 bytes for IP address
    - 6 bytes for MAC address
    
    Create fixed-lenght header: 
    the first 12 bytes are ethernet header  +
    then 8 byte for ip header               +
    then 11 bytes for application header    =
                                            31 bytes
    MAC_SRC, MAC_DST, IP_SRC, IP_DST, MSG_LEN, TYPE

    then concatenate MSG
    '''
    #6 byte representation of mac_src and mac_dst
    mac_src_packet = unhexlify(mac_src.replace(':',''))
    mac_dst_packet = unhexlify(mac_dst.replace(':',''))
    #4 byte representation of ip_src and ip_dst
    ip_src_packed = inet_aton(ip_src)
    ip_dst_packed = inet_aton(ip_dst)

    #Convert to byte the remaining part
    string_part = f"{len(msg):<{H_MSGLEN}}{msg_type:<{H_TYPELEN}}{msg}".encode()

    #Join all the bytes and return the message
    return b"".join([mac_src_packet, mac_dst_packet, ip_src_packed, ip_dst_packed, string_part])
    

def get_header_info(message):
    '''
    Get all the information from the header network message.
    return [mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type]
    '''    
    #Convert MAC address
    mac_src = convertMacToStr(message[0:6])
    mac_dst = convertMacToStr(message[6:12])

    #Convert 4-bytes representation of IP address to String
    ip_src = inet_ntoa(message[12:16])
    ip_dst = inet_ntoa(message[16:20])

    #Convert the remaing part
    string_part = message[20:20+H_MSGLEN+H_TYPELEN].decode()
    msg_len = int(string_part[0:H_MSGLEN])
    msg_type = int(string_part[H_MSGLEN : H_MSGLEN+H_TYPELEN])

    return [mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type]

def convertMacToStr(macBytes):
    '''
    Convert the 6-bytes representation of MAC address to String
    '''
    macBytes = str(hexlify(macBytes).decode())
    mac_inverse = ':'.join(macBytes[i:i+2] for i in range(0, len(macBytes), 2))
    return (mac_inverse.upper())





#Test module
if __name__ == "__main__":
    #Check if header is 31 bytes
    print(len(create_message("10:AF:CB:EF:19:CF","15:AA:AA:AA:32:AA","192.168.1.1","192.168.1.2","9","")))
    msg = create_message("10:AF:CB:EF:19:CF","15:AA:AA:AA:32:AA","192.168.1.1","192.168.1.2","2","ciaaao xooxoxoxo loooool")
    print(msg)
    print(get_header_info(msg))