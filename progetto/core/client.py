from socket import *
from threading import Thread, RLock
from socket import inet_aton
import core.utilities as utilities
import json

class Client:
    '''
    A class that represent a client connected to the network
    The client can send message to another client specifing the IP address of destination
    The client also handle the return of a message if the dst_client isn't online.
    To distinguish the messages, client creates message in this way:
    msg = [ID msg]
    The msg is converted in JSON format and sent.
    The ID is simple an integer, but in future may introduce some more secure and complicated stuff
    '''
    def __init__(self, ip, mac, router_ip, router_mac, router_port):
        '''
        ip -> client ip address
        mac -> client mac address
        router_ip -> router ip address
        router_mac -> router mac address
        router_router -> router welcome port for new client connections
        ''' 
        self.__ip = ip
        self.__mac = mac
        self.__router_ip = router_ip
        self.__router_mac = router_mac
        #Set-up socket with the router interface
        self.__sock = socket(AF_INET, SOCK_STREAM)
        self.__sock.connect(('',router_port))

        #Send to the router client ip and mac addresses
        self.__sock.send(utilities.create_message(mac,router_mac,ip, router_ip, utilities.CLIENT_A, "A"))

        #Message sent history in order to inform user which message is interested when receiving errors
        #Save id -> [ip_dst]
        self.__messages = {}

        #Lock for the messages
        self.__m_lock = RLock()

    def start(self):
        '''
        Principal method that runs client code
        No need to use locks on socket because sockets synchronization on different thread
        is managed by the OS
        '''
        th1 = Thread(target = self.__handle_new_message)
        th1.setDaemon(True)
        th1.start()
        #The main thread handle received message
        self.__handle_recived_message()

    def __handle_new_message(self):
        '''
        Method that handle new message that client want send to the user
        '''
        print('\033[94m' + "WELCOME CLIENT " + self.__ip + '\033[0m' + ", message format is IP:MSG\n"+ '\033[92m' +"Example:\n192.168.1.10:Hello World\n" + '\033[0m')
        id_msg = 0
        while True:
            msg = input()
            data = msg.split(':')
            #Check that msg is composed by two part
            if(len(data) != 2):
                print('\033[91m' +"Message format error, example of correct one:\n192.168.1.10:Hello World\n" + '\033[0m')
                continue

            ip_dst = data[0]
            message = data[1]
            #Check that the specified IP is valid
            ip_data = ip_dst.split(".")
            #Check IP lenght
            if(len(ip_data) != 4 or len(ip_data[0]) > 3 or len(ip_data[1]) > 3 or len(ip_data[2]) > 3 or len(ip_data[3]) > 3):
                #IP NOT VALID
                print('\033[91m' +"IP error, Enter a valid IP, example of correct message format:\n192.168.1.10:Hello World\n" + '\033[0m')
                continue

            #IP validation
            try:
                temp = inet_aton(ip_dst)
            except:
                print('\033[91m' +"IP error, Enter a valid IP, example of correct message format:\n192.168.1.10:Hello World\n" + '\033[0m')
                continue
                
            #If the message is in the correct-form let's send it to the dst
            #Build the complete message, with the ID in orther to recognise them if it fail to be sent.
            message = [id_msg, message]
            #Conver msg object in JSON
            json_string = json.dumps(message)
            self.__sock.send(utilities.create_message(self.__mac, self.__router_mac, self.__ip, ip_dst, utilities.NEW_MSG, json_string))
            #Insert msg in the dictionary
            self.__m_lock.acquire()
            self.__messages[id_msg] = ip_dst
            self.__m_lock.release()
            #Increase ID counter
            id_msg += 1
    
    def __handle_recived_message(self):
        '''
        Method that handle message received from the socket, they can be two different types:
        1) Message from another client
        2) Error message from the server, a previous sent (recognized by ID) message can't be sent to the client
           becouse the dst_client is offline
        '''
        headerTime = True
        fullmsg = b''
        msg = b''

        while True:
        #Read new part of message from socket
            try:
                new_part = self.__sock.recv(32)
                if(len(new_part) == 0):
                    #Connection lost
                    print("DISCONNECTED")
                    self.__sock.close()
                    exit(0)
            except:
                print("DISCONNECTED")
                exit()

            #Append bytes of the new message
            msg = b"".join([msg,new_part])

            while len(msg) > 0:
                if headerTime:
                    #Extract all the information from header.
                    mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type = utilities.get_header_info(msg)
                    headerTime = False
                    msg = msg[31:]
                
                '''Read only the part containing the rest of the message.. The other part may be the header
                of the next message in the stream, if there are byte (so a part) of the next header,
                it will exit by the cycle because headerTime will be True and the remaing part will be
                appended by the join method.'''
                temp = msg_len - len(fullmsg)
                fullmsg = b"".join([fullmsg,msg[:temp]])
                msg = msg[temp:]

                if (len(fullmsg)) >= msg_len : 
                    #All message is read
                    headerTime = True
                    #Send message to the dst client
                    self.__handle_message(ip_src, msg_type, fullmsg[:msg_len].decode())
                    fullmsg = b''
                    break

    def __handle_message(self, ip_src, msg_type, msg):
        data = json.loads(msg)
        #data[0] -> ID
        #data[1] -> msg
        if msg_type == utilities.NEW_MSG:
            #New message from client (it can be also this client if it send a message to itself obvioulosy)
            print("\n" + '\033[93m' + "New Message from " + ip_src + '\033[0m' + '\033[93m' + ": " + '\033[0m' + data[1])
        elif msg_type == utilities.ERR_MSG:
            self.__m_lock.acquire()
            ip_dest = self.__messages[data[0]]
            self.__m_lock.release()
            print("\n" + '\033[91m' + "ERROR:" + '\033[0m' + " message to " + '\033[94m' + ip_dest + '\033[0m' + ", containing: \"" + '\033[93m' + data[1] + '\033[0m' + "\" was not send, client may be offline, or ip not in network\n")



