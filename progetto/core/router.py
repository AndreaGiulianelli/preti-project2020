'''
Router class for all routers
A router has two sockets:
1) A socket that let the client to connect with him.
2) A socket to communicate with the server

It has also to:
- handle message from clients: send msg to server
- handle message from server
'''

from socket import *
from threading import Thread, RLock
import core.utilities as utilities

class Router:

    def __init__(self, ip, mac, server_ip, server_mac, server_port):
        '''
        Setting up and start socket for server communications.
        IP and MAC are the address of the interface that is on the server network
        server_port is the port of the welcome server port for new routers
        '''
        self.__eth_to_sever = RouterInterface(ip, mac)
        self.__server_ip = server_ip
        self.__server_mac = server_mac
        self.__server_port = server_port
        self.__server_socket = socket(AF_INET, SOCK_STREAM)
        
        #Lock to ensure mutual exclusion when accessing the serversocket
        #self.__server_socket_lock = RLock()

        #Dict that will containt client sockets
        #ip_client -> socket
        self.__client_sockets = {}

        #Lock to ensure mutual exclusion when accessing the client socket dict
        self.__client_sockets_lock = RLock()
    
    def start(self):
        #Start method, call it when all is setUp
        self.__server_socket.connect(('',self.__server_port ))
        #Send to the server our ip and mac address in order to configure server's internal table
        self.__server_socket.send(utilities.create_message(self.__eth_to_sever.get_mac(),self.__server_mac,self.__eth_to_sever.get_ip(), self.__server_ip, utilities.ROUTER_A, "A"))

        #Handle server socket message
        self.__handle_message_from_server()


    def __handle_router_interface(self, interface_socket, eth_to_clients):
        while True:
            #New client has connected
            client_socket, address = interface_socket.accept()
            #Wait for the client's ip and mac, it is exactly a chunck of 32 bytes
            msg = client_socket.recv(32)
            if(len(msg) == 0):
                #Connection lost, client isn't saved
                continue
            [mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type] = utilities.get_header_info(msg)

            #Save client socket in client_sockes dict
            address = ip_src
            self.__client_sockets_lock.acquire()
            #ip -> [mac, socket]
            self.__client_sockets[address] = [mac_src, client_socket]
            self.__client_sockets_lock.release()

            #Launch a new Thread to manage the client
            th = Thread(target = self.__handle_client_socket, args = [eth_to_clients, client_socket, address,])
            th.setDaemon(True)
            th.start()

    def set_client_interface(self,ip, mac, welcome_port):
        '''
        Setting up and start socket for client connections.
        once the client connects, router has to inform the server that a new client has connected
        then the router has to handle message from clients directed to server.
        In this method router can have multiple sockets to client if we want to expand program without fixed ip and mac.
        Also the router can have multiple client_interface, as the router is connected to varius switch with different client, so more than one as the figure in the "Traccia 1"
        
        For example:
        r1 = Router("195.1.10.1", "55:04:0A:EF:10:AB","195.1.10.10","52:AB:0A:DF:10:DC",8100) #Set the router and the server connection
        r1.set_client_interface("92.10.10.1", "55:04:0A:EF:11:CF", 8000) #Set the first socket for client connections
        r1.set_client_interface("93.10.10.1", "AA:04:0A:EF:11:CA", 7999) #Set the second socket for client connections
        Router accepts client from two "ideal switches" and we can add more and more switch to the router.
        This make this program expandable as we want and not fixed to the "Traccia 1" example
        '''
        #Router interface where clients connect
        eth_to_clients = RouterInterface(ip, mac)
        #Create welcome socket for TCP connections
        sock = socket(AF_INET, SOCK_STREAM)
        sock.bind(('',welcome_port))
        sock.listen(5)

        #Handle the interface
        th = Thread(target = self.__handle_router_interface, args = [sock, eth_to_clients,])
        th.setDaemon(True)
        th.start()

    def __send_message_to_server(self, ip_src, ip_dst, msg_type, msg):
        self.__server_socket.send(utilities.create_message(self.__eth_to_sever.get_mac(), self.__server_mac, ip_src, ip_dst, msg_type,msg))


    def __send_message_to_client(self, ip_src, ip_dst, mac_dst, msg_type, msg):
        print([ip_src, ip_dst, msg_type, msg])
        #Now send the message to the client socket
        #Fist check that the client is connected to the router, if not discard the message
        self.__client_sockets_lock.acquire()
        if ip_dst in self.__client_sockets:
            #Ok, client is connected
            #Get the mac_dst
            router_interface_mac = self.__client_sockets[ip_dst][0]
            #Get the socket to communicate with the client
            router_interface_socket = self.__client_sockets[ip_dst][1]
            #Send message to the client
            router_interface_socket.send(utilities.create_message(router_interface_mac, mac_dst, ip_src, ip_dst, msg_type, msg))
        self.__client_sockets_lock.release()

    def __handle_message_from_server(self):
        headerTime = True
        fullmsg = b''
        msg = b''

        while True:
            #Read new part of message from socket
            try:
                new_part = self.__server_socket.recv(32)
                if(len(new_part) == 0):
                    print("DISCONNECTED")
                    self.__server_socket.close()
                    exit(0)
            except:
                print("DISCONNECTED")
                self.__server_socket.close()
                exit(0)

            #Append bytes of the new message
            msg = b"".join([msg,new_part])

            while len(msg) > 0:
                if headerTime:
                    #Extract all the information from header.
                    mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type = utilities.get_header_info(msg)
                    headerTime = False
                    msg = msg[31:]
                
                '''Read only the part containing the rest of the message.. The other part may be the header
                of the next message in the stream, if there are byte (so a part) of the next header,
                it will exit by the cycle because headerTime will be True and the remaing part will be
                appended by the join method.'''
                temp = msg_len - len(fullmsg)
                fullmsg = b"".join([fullmsg,msg[:temp]])
                msg = msg[temp:]

                if (len(fullmsg)) >= msg_len : 
                    #All message is read
                    headerTime = True
                    #Send message to the dst client
                    self.__send_message_to_client(ip_src,ip_dst, mac_dst, msg_type,fullmsg[:msg_len].decode())
                    fullmsg = b''
                    break

    def __disconnected_client(self, address):
        #Delete client from client_sockets of clients online
        self.__client_sockets_lock.acquire()
        if address in self.__client_sockets:
            del self.__client_sockets[address]
        self.__client_sockets_lock.release()
        #Alert server that client connection is broken
        self.__send_message_to_server(self.__eth_to_sever.get_ip(), self.__server_ip, utilities.CLIENT_DISC, address)

    def __handle_client_socket(self, router_interface, socket, address):
        #First inform the server that new client has connected to the network
        self.__send_message_to_server(self.__eth_to_sever.get_ip(), self.__server_ip, utilities.NEW_CLIENT, address)

        #Now this is the socket that connect the router to the client, if arrives other messages from the client
        #they are sent straight to the server.
        headerTime = True
        fullmsg = b''
        msg = b''

        while True:
            #Read new part of message from socket
            try:
                new_part = socket.recv(32)

                if(len(new_part) == 0):
                    #if len(new_part) equals zero, the connection is broken, so alert server, cancel client sockets and return
                    self.__disconnected_client(address)
                    socket.close()
                    return
            except:
                #Also, if there is an exception the connection is broken, so alert server, cancel client sockets and return
                self.__disconnected_client(address)
                socket.close()
                return

            #Append bytes of the new message
            msg = b"".join([msg,new_part])

            while len(msg) > 0:
                if headerTime:
                    #Extract all the information from header.
                    mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type = utilities.get_header_info(msg)
                    headerTime = False
                    msg = msg[31:]
                
                '''Read only the part containing the rest of the message.. The other part may be the header
                of the next message in the stream, if there are byte (so a part) of the next header,
                it will exit by the cycle because headerTime will be True and the remaing part will be
                appended by the join method.'''
                temp = msg_len - len(fullmsg)
                fullmsg = b"".join([fullmsg,msg[:temp]])
                msg = msg[temp:]

                if (len(fullmsg)) >= msg_len : 
                    #All message is read
                    headerTime = True
                    #Send message to the server
                    self.__send_message_to_server(ip_src,ip_dst, msg_type,fullmsg[:msg_len].decode())
                    fullmsg = b''
                    break


class RouterInterface:

    def __init__(self,ip,mac):
        self.__ip = ip
        self.__mac = mac
    
    def get_ip(self):
        return self.__ip

    def get_mac(self):
        return self.__mac