#ADDITIONAL CLIENT CONNECTED TO INTERFACE 192.168.1.1 OF ROUTER3 TO PROVE THE EXTENDIBILITY
from socket import *
from core.client import Client

ip = "192.168.1.2"
mac = "44:BF:5B:DA:11:AC"
router_ip = "192.168.1.1"
router_mac = "34:10:FA:3F:12:AA"
router_port = 8300

client = Client(ip, mac, router_ip, router_mac, router_port)
client.start()