from socket import *
from core.client import Client
from signal import signal, SIGINT
from sys import exit

ip = "92.10.10.15"
mac = "32:04:0A:EF:19:CF"
router_ip = "92.10.10.1"
router_mac = "55:04:0A:EF:11:CF"
router_port = 8000

client = Client(ip, mac, router_ip, router_mac, router_port)
client.start()