'''
Server
The server has to:
- handle client connections: it mantains a table with the online client.
- foward client message

'''
from socket import *
from threading import Thread, RLock
import core.utilities as utilities

SERVER_IP = "195.1.10.10"
SERVER_MAC = "52:AB:0A:DF:10:DC"

#dict of router sockets, the ip and the associated socket
routers = {}

#dict of foward table client_ip -> router_ip
foward_table = {}

#Lock for routerTable
r_lock = RLock()

#Lock for foward table
f_lock = RLock()

def handle_message(mac_src, mac_dst, ip_src, ip_dst, msg_type, msg): 
    global foward_table
    print(f"Messaggio: {ip_src} {mac_src} {ip_dst} {mac_dst} {msg_type} {msg}")
    if(msg_type == utilities.NEW_CLIENT):
        #New client connected
        f_lock.acquire()
        foward_table[msg] = ip_src
        print(foward_table)
        f_lock.release()
        pass
    elif(msg_type == utilities.CLIENT_DISC):
        #Client disconnected
        f_lock.acquire()
        #So if the client was present in the foward table, delete it
        if msg in foward_table:
            del foward_table[msg]
            print(foward_table)
        f_lock.release()
        pass
    elif(msg_type == utilities.NEW_MSG):
        #Client to client message
        #First check that the dst is online
        f_lock.acquire()
        if ip_dst in foward_table:
            #Then send the msg to the correct router
            '''
            with x = foward_table[ip_dst] get the router ip to foward the message
            with router[x] get the list [router_mac, router_socket]
            '''
            r_lock.acquire()
            router_dst_mac = routers[foward_table[ip_dst]][0]
            send_socket = routers[foward_table[ip_dst]][1]
            #Foward the message to the correct router in order to reach destination
            send_socket.send(utilities.create_message(SERVER_MAC, router_dst_mac, ip_src, ip_dst, utilities.NEW_MSG, msg))
            r_lock.release()
        else:
            r_lock.acquire()
            #if the dst isn't active, return the msg to the source with an error message
            router_src_mac = routers[foward_table[ip_src]][0]
            send_socket = routers[foward_table[ip_src]][1]
            #Foward the message to the correct router in order to reach destination
            send_socket.send(utilities.create_message(SERVER_MAC, router_src_mac, SERVER_IP, ip_src, utilities.ERR_MSG, msg))
            r_lock.release()
        f_lock.release()

def delete_router(address):
    global foward_table
    global routers
    #Delete router from router table.
    r_lock.acquire()
    if address in routers:
        del routers[address]
    r_lock.release()
    #Delete all client connected to the router, because they are unreachable
    f_lock.acquire()
    foward_table = {key:value for(key, value) in foward_table.items() if value != address}
    f_lock.release()

#Function with which server handle all the message from one router
def handle_router(socket, address):
    headerTime = True
    fullmsg = b''
    msg = b''

    while True:
        #Read new part of message from socket
        try:
            new_part = socket.recv(32)
            if(len(new_part) == 0):
                #if len(new_part) equals zero, the connection is broken, so return
                delete_router(address)
                socket.close()
                return
        except:
            #Also, if there is an exception the connection is broken, so return
            delete_router(address)                
            socket.close()
            return 

        #Append bytes of the new message
        msg = b"".join([msg,new_part])

        while len(msg) > 0:
            if headerTime:
                #Extract all the information from header.
                mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type = utilities.get_header_info(msg)
                headerTime = False
                msg = msg[31:]
            
            '''Read only the part containing the rest of the message.. The other part may be the header
            of the next message in the stream, if there are byte (so a part) of the next header,
            it will exit by the cycle because headerTime will be True and the remaing part will be
            appended by the join method.'''
            temp = msg_len - len(fullmsg)
            fullmsg = b"".join([fullmsg,msg[:temp]])
            msg = msg[temp:]

            if (len(fullmsg)) >= msg_len : 
                #All message is read
                headerTime = True
                handle_message(mac_src, mac_dst, ip_src, ip_dst, msg_type,fullmsg[:msg_len].decode())
                fullmsg = b''
                break


#Create the welcome socket for TCP connections
welcome_socket = socket(AF_INET, SOCK_STREAM)
welcome_socket.bind(('',8100))
welcome_socket.listen(5)

while True:
    try:
        router_socket, address = welcome_socket.accept()
    except:
        print("DISCONNESSO")
        welcome_socket.close()
        exit(0)
    #Wait for the router's ip and mac, it is exactly a chunk of 32 bytes
    msg = router_socket.recv(32)
    if(len(msg) == 0):
        #Connection lost, router isn't saved
        continue
    [mac_src, mac_dst, ip_src, ip_dst, msg_len, msg_type] = utilities.get_header_info(msg)
    
    #Save the router socket in the routers dict
    r_lock.acquire()
    address = ip_src
    #ip -> [mac, socket]
    routers[address] = [mac_src , router_socket]
    print(routers)
    r_lock.release()

    #Launch new Thread to manage the router
    th = Thread(target = handle_router, args=[router_socket, address,])
    th.setDaemon(True)
    th.start()

