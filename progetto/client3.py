from socket import *
from core.client import Client

ip = "92.10.10.25"
mac = "AF:04:67:EF:19:DA"
router_ip = "92.10.10.2"
router_mac = "AA:04:0A:EF:11:CA"
router_port = 8000

client = Client(ip, mac, router_ip, router_mac, router_port)
client.start()
