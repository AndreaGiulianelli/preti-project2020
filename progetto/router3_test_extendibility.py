#ADDITIONAL ROUTER TO PROVE THE EXTENDIBILITY

from core.router import *

r1 = Router("195.1.10.3", "57:04:0A:EF:AA:AA","195.1.10.10","52:AB:0A:DF:10:DC",8100)
r1.set_client_interface("192.168.1.1", "34:10:FA:3F:12:AA", 8300)
#I can add more interface for clients.
r1.set_client_interface("192.168.2.1", "34:10:FA:DF:12:AA", 8400)
r1.start()