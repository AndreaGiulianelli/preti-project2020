from socket import *
from core.client import Client

ip = "1.5.10.15"
mac = "42:A3:1B:DA:12:AC"
router_ip = "1.5.10.1"
router_mac = "32:03:0A:DA:11:DC"
router_port = 8200

client = Client(ip, mac, router_ip, router_mac, router_port)
client.start()
