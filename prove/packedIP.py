import socket
import binascii

def convertIP(ip):
    packed = socket.inet_aton(ip)
    unpacked = socket.inet_ntoa(packed)
    return (unpacked, packed)

[unpack, pack] = convertIP('192.168.1.1')
print(f"{len(pack)}: {pack}; {len(unpack)}: {unpack}")
mac = '10:AF:CB:EF:19:CF'.replace(':','');
macBytes = binascii.unhexlify(mac)
print(f"{len(macBytes)} : {macBytes}")


prova = b"".join([macBytes,macBytes,pack,pack])
print(f"{len(prova)} : {prova}")

#RICONVERTO MAC ADDRESS
macBytes = str(binascii.hexlify(macBytes).decode())
mac_inverse = ':'.join(macBytes[i:i+2] for i in range(0, len(macBytes), 2))
print(mac_inverse.upper())


