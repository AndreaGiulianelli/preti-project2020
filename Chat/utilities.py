H_MSGSIZE=10
H_IDSIZE=5

def createMessage(idClient, msg):
    return (f"{len(msg):< {H_MSGSIZE}}" + f"{idClient:<{H_IDSIZE}}" + msg).encode()


def extractHeader(msg):
    '''
        return header
        [msgsize, id]
    '''
    return [int(msg[:H_MSGSIZE]), int(msg[H_MSGSIZE : H_MSGSIZE+H_IDSIZE])]