from socket import *
from threading import Thread
import time
from person import Person
import utilities 

def sendBroadcast(clientID,msg):
    for client in clients:
        message = utilities.createMessage(clientID,msg)
        client.send(message)

def handleClient(clientSocket):
    headerTime = True
    fullmsg = ''

    while True:
        msg = clientSocket.recv(16)
        msg = msg.decode()
        
        while len(msg) > 0:
            if headerTime:
                msgLen, client = utilities.extractHeader(msg)
                headerTime = False
            fullmsg += msg
            msg = ''
            if (len(fullmsg) - utilities.H_MSGSIZE - utilities.H_IDSIZE) >= msgLen : 
                headerTime = True
                sendBroadcast(client, fullmsg[utilities.H_MSGSIZE + utilities.H_IDSIZE:utilities.H_MSGSIZE + utilities.H_IDSIZE + msgLen])
                msg = fullmsg[utilities.H_MSGSIZE + utilities.H_IDSIZE + msgLen:]
                fullmsg = ''


serverSocket = socket(AF_INET, SOCK_STREAM)
serverAddr = ('',5500)
serverSocket.bind(serverAddr)
serverSocket.listen(5)

idClient = 0
clients = []

while True:
    clientSocket, address = serverSocket.accept()
    idClient += 1
    print(f"Conn: {address}, id: {idClient}, send id...")
    #ACK communication with the ID
    clientSocket.send(utilities.createMessage(idClient, ""))
    clients.append(Person(idClient, clientSocket))
    th = Thread(target = handleClient , args=(clientSocket,))
    th.start()

    