from socket import *
from threading import Thread, Event, Lock
import utilities
import time

def displayMsg(client, msg):
    if(client != myId):
        print(f"user{client}: {msg}")
    else:
        print(f"YOU: {msg}")

def handleMessageFromServer():
    global myId
    headerTime = True
    fullmsg = ''
    while True:
        headerTime = True
        fullmsg = ''
        while True:
            msg = clientSocket.recv(16)
            msg = msg.decode()

            #Finche ho messaggi dentro il chunks ricevuto continuo ad elaborare
            while len(msg) > 0:
                 #Se aspetto l'header di un nuovo messaggio lo leggo
                if headerTime:
                    msgLen, client = utilities.extractHeader(msg)
                    headerTime = False
                #if the msg was the ACK of the communication to server
                if msgLen == 0:
                    myId = client     
                    e.set()   
                                 
                
                fullmsg += msg
                msg = ''
                #Controllo se ho letto tutti i dati relativi quel messaggio
                if (len(fullmsg) - utilities.H_MSGSIZE - utilities.H_IDSIZE) >= msgLen : 
                    headerTime = True
                    #Message ready
                    if(msgLen > 0):
                        displayMsg(client, fullmsg[utilities.H_MSGSIZE + utilities.H_IDSIZE: utilities.H_MSGSIZE + utilities.H_IDSIZE + msgLen])
                    #Se all'interno dell'ultimo chunks c'era una parte anche del messaggio successivo allora reimposto msg
                    #in questo modo alla prossima iterazione invece di rifare la recv perdendo dati, leggerá questi.
                    msg = fullmsg[utilities.H_MSGSIZE + utilities.H_IDSIZE + msgLen:]
                    fullmsg = ''


myId = 0
e = Event()
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect(('',5500))
th = Thread(target = handleMessageFromServer)
th.start()

#Wait that server send me the id
e.wait()
print(f"My ID is {myId}")

#The principal thread handle user input
while True:
    msg = input()
    clientSocket.send(utilities.createMessage(myId, msg))


